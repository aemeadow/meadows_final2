﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPlayer : MonoBehaviour


{
    public KeyCode teleportInput = KeyCode.LeftAlt;

     public bool OnAlt = true;

      public GameObject thePlayer;
    
     protected virtual void InputHandle()
        {
            TeleportInput();
        }

    protected virtual void TeleportInput()
        {
            if (Input.GetKeyDown(teleportInput))
            { 
                if(OnAlt == true)
                {
                    OnAlt = false;
                    thePlayer.transform.Translate (200,15,200);
                }
                else 
                {
                    OnAlt = true;
                    thePlayer.transform.Translate (-200,15,-200);
                }
            }
              

        }


}
